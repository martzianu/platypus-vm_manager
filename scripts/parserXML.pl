use strict;
use warnings;
use diagnostics;
use autodie;

use Data::UUID;
use XML::Twig;
use Path::Class;


my $infile = "../templates/standard_EFI_0_1_1.xml";
my @now = localtime();
my $timestamp = sprintf("%04d%02d%02d%02d%02d%02d", 
                        $now[5]+1900, $now[4]+1, $now[3],
                        $now[2],      $now[1],   $now[0]);
my $dir = dir("../run/active/");
my $outfile = $dir-> file("vm-$timestamp.xml") ;
my $outfilehandle = $outfile -> openw();    

my $efionoff = "0" ; # 0 for BIOS and 1 for EFI
my $diskdrive = "PLACEHOLDER"; #path to raw disk file
my $isodrive = "PLACEHOLDER"; #path to iso


my $twig = XML::Twig->new( 
	pretty_print => 'indented',
	twig_handlers => {
		name => \&namegen,
        uuid => \&uuid,
		domain => \&domain,
	 	disk => \&set_path,
        os => \&bios,
        },
);

$twig -> parsefile( $infile );
$twig->print($outfilehandle);

###Subroutines###
sub namegen {
    my ( $twig , $namegen) = @_;
    $namegen->set_text($timestamp);
}

sub domain  {
    my ( $twig, $domain) =  @_;
    my $idno = int(rand(999999));
    $domain->set_att( "id" => $idno);
}

sub uuid    {
    my ($twig , $uuid) = @_;
    my $ug = Data::UUID->new;
	my $ci = $ug->create_str();
    $uuid->set_text($ci);
}

sub set_path	{
	my ($twig, $set_path) = @_;
	my @dlist = $set_path->att('device');
	foreach my $path (@dlist){
		if ($path eq 'disk' ) {
			$set_path->last_child("source")->set_att( "file" => "$diskdrive");
		}
		elsif ($path eq 'cdrom' ) {
			$set_path->last_child("source")->set_att( "file" => "$isodrive");
		}
		else { last; }		# exit
	}
}


sub bios    {
    my ($twig , $bios ) = @_;
    if ($efionoff == 0) { 
    $bios->last_child("loader")->delete();
    $bios->last_child("nvram")->delete();
    }
}
